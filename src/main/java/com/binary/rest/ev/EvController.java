package com.binary.rest.ev;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 提供通用的查询方法 对不同的Repository 只要指定 EvParam里的实体类名称就能查询该表了
 */
@RequestMapping("/ev")
@RestController
public class EvController {
    @Autowired
    private EvService evService;

    @PostMapping("/list")
    public List list(@RequestBody EvParam evParam) {
        return evService.list(evParam);
    }

    @PostMapping("/page")
    public Page page(@RequestBody EvParam evParam) {
        return evService.page(evParam);
    }

    @PostMapping("/save")
    public Object save(@RequestBody EvParam evParam) {
        return evService.save(evParam);
    }

    @PostMapping("/delete")
    public void deleteById(String entityName, String id) {
        evService.deleteById(entityName, id);
    }

    @GetMapping("/get")
    public Object getById(String entityName, String id) {
        return evService.getById(entityName, id);
    }

}

