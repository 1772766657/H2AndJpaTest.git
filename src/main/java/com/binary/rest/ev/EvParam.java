package com.binary.rest.ev;

import lombok.Data;

import java.util.List;
import java.util.Map;


@Data
public class EvParam {
    private String entityName;
    private String exampleJson;
    private Integer page;
    private Integer size;
    private List<Map<String, String>> sorts;
}
