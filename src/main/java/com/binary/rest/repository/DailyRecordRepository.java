package com.binary.rest.repository;



import com.binary.rest.entity.DailyRecord;
import org.springframework.data.jpa.repository.JpaRepository;


public interface DailyRecordRepository extends JpaRepository<DailyRecord, String> {

}
