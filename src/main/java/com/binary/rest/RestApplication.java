package com.binary.rest;

import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

@SpringBootApplication
public class RestApplication {
    private static final Logger log = LoggerFactory.getLogger(RestApplication.class);

    static {
        try {
            InputStream inputStream = RestApplication.class.getClassLoader().getResourceAsStream("application.yml");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = null;
            while ((line = (reader.readLine())) != null) {
                if (line.matches(".*jdbc:h2:tcp:.*")) {
                    String port = line.replaceAll("^.*tcp://.*?:(\\d+)/.*$", "$1");
                    Server server = Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort",
                            port);
                    log.info("启动H2数据库.... {}", line);
                    server.start();
                }
            }
        } catch (Exception e) {
            log.error("启动H2服务报错 ... {}", e.getMessage());
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        SpringApplication.run(RestApplication.class, args);
    }

}
