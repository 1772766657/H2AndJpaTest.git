package com.binary.rest.util;


public class SnowflakeUtil {

    private static SnowflakeIdWorker worker = null;

    static {
        //默认
        config(0L, 0L);
    }

    public synchronized static void config(long workId, long dataCenterId) {
        if (workId > 32 || workId < 0) {
            throw new RuntimeException("WorkId 请设置在 0~31之间");
        }
        if (dataCenterId > 32 || dataCenterId < 0) {
            throw new RuntimeException("dataCenterId 请设置在 0~31之间");
        }
        worker = new SnowflakeIdWorker(dataCenterId, workId);
    }

    public static long getIdLong() {
        return worker.nextId();
    }

    public static String getIdString() {
        return String.valueOf(worker.nextId());
    }
}
