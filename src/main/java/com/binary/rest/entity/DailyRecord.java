package com.binary.rest.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class DailyRecord {
    @Id
    private String id;
    private String groupId;
    private String key;
    private String value;

}
